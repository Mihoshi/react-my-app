import React, {useState} from "react";
import {Checkbox,List,ListItem,ListItemText} from "@material-ui/core";

//一覧するデータを状態（state）として記述　通常はサーバから取ってくるが今回は直接記述
const initialItems = [
    //”contents”の内容がListコンポーネントに表示される
    //チェックボックスにチェックすると”checked”が変更される
    {id: 1, contents:'やること1', checked: false},
    {id: 2, contents:'やること2', checked: true},
    {id: 3, contents:'やること3', checked: false},
    {id: 4, contents:'やること4', checked: false}
];

const ItemList = () => {
    const [items, setItems] = useState(initialItems);
    const toggleChecked = (item) => {
        //itemはリスト内の1行分のデータ　外側の引数は内側から参照できる
        //戻り値はチェックボックスをON/OFFにした際に動作する関数
        return () => {
            //一覧データから対象データを検索　
            const targetIndex = items.indexOf(item);
            //対象データのチェック状態を変更　ONだったならOFFへ、OFFだったならONへ
            items[targetIndex].checked = ! items[targetIndex].checked;
            //状態(state)に変更後の一覧データを、変更用の関数setItemsを使って反映させる
            setItems([...items]);
        };
    }
    return(
        <List>
            {items.map( item => (
                //
                <ListItem key={item.id}>
                    {/**/}
                    <Checkbox
                        checked={item.checked}
                        onChange={toggleChecked(item)}
                        />
                    {/*
                    */}
                        <ListItemText primary={item.contents} />
                </ListItem>
            ))}
        </List>
    );
};
export default ItemList;