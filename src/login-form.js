import React from "react"; //node_modules配下のライブラリなどはパスではなく名前だけ記述しておけば良い
import {Paper, TextField, Typography, Button} from "@material-ui/core"; //利用するコンポーネントをインポート Reactで用意されているものなので先頭大文字
import { makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    container: {
        display: 'flex',height: '100vh',backgroundColor: '#eeeeee'
    },
    loginBase: {
        width: '300px',height: '200px',margin: 'auto',padding: '10px'
    },
    field: {width: '80%'},
    loginButton: {margin: '15px'}
});

const LoginForm = () => {  //自作するコンポーネントは基本的に先頭大文字 小文字では動かない 小文字ではhtmlでの表現になる
   const classes = useStyles();
    return ( //Buttonコンポーネントを利用　variantは外見指定、colorは文字色指定 material-ui.com/components/buttons/ にどうなるか載っている
        <div className={classes.container}>
            <Paper className={classes.loginBase}>
                <Typography>ログインしてください。</Typography>
                <TextField className={classes.field} label='Name'/>
                <TextField className={classes.field} label='Password'/>
                <Button className={classes.loginButton} variant='contained' color='primary'>
                    ログイン
                </Button>
            </Paper>
        </div>
    );
};
export default LoginForm;